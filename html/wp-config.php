<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'aa177qbfrg_wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'aa177qbfrg');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'xfX22fzB');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Yr`DW:%>%6E7/f@%q`jtlE4vBAg&_d;!Q+q%Sm;1lF~u,kO;*g>8Ftpj9nc|KA2]');
define('SECURE_AUTH_KEY',  '+]TlgF1;v o+*Dj)E*nO(`~X6>fJd)Tb* M]~6Zc{ ]Jm{PA%Jx<:hq(<cU5Xzia');
define('LOGGED_IN_KEY',    '[K3qy-9 _.{Ws(g!xV;b;g> Hz4j(*#`p%hFYvp?|$wepH_pZ,8R@ow-;u+o9oWY');
define('NONCE_KEY',        ')B}Is$Co/7YRlD>3sE j9yS:Rxq;k#]K.g)%OTY$j2Wlg-; 9AxTuy07lxiGCN^R');
define('AUTH_SALT',        '#M,f[0^5,/}L9!X}=rt;Pkeb+Fx&u[RFe;$21D)~G7%f@.GcjN%uerKmq<R:z)0i');
define('SECURE_AUTH_SALT', '55*_N9p_z:lx+m7wtQ3B*JSe6A,*7mMKzCJp6Kv-BgRD`xxC@:4WPjQe}&.{Z&<C');
define('LOGGED_IN_SALT',   '8Qb$<3/;hUQHS_l.gQ{_hBth8MF; 1>$w5x):Ywe0`[=js:Uex9J)ML~.-igF^U|');
define('NONCE_SALT',       '-C5<q:iWuko1:2kIR+j<f3[UIFqEt)l@`OPD6lg8 X.xLn(c~_)y@Bl$`uORI)o-');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
