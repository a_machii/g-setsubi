<?php /* Template Name: page */ ?>
<?php
  $page_id = $post->ID; //xxxに 固定ページIDを入力
  $content = get_page($page_id);
  $this_page_title = $content->post_title;
  $this_page_slug = $content->post_name;
?>
<?php get_header(); ?>

    <nav id="breadcrumb">
      <ol>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href="<?php echo home_url('/'); ?>"><span itemprop="title">HOME</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
          <span itemprop="title"><?php echo $this_page_title; ?></span>
        </li>
      </ol>
    </nav>

    <div id="wrap">
      <main id="<?php echo $this_page_slug; ?>" class="main low">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
      </main><!-- /#<?php echo $this_page_slug; ?> .main .low -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>