<?php get_header(); ?>

    <div id="wrap">
      <main id="top" class="main">
        <section id="tproduct" class="tsection">
          <h2 class="section_ttl"><img src="<?php echo home_url('/'); ?>assets/img/top/tproduct_ttl.gif" width="127" height="45" alt="事業案内"></h2>
          <ul>
            <li>
              <section>
                <p class="img"><a href="<?php echo home_url('/'); ?>works#unit01" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/img01.jpg" width="215" height="130" alt="電気・設備工事の写真"></a></p>
                <h3><a href="<?php echo home_url('/'); ?>works#unit01">電気・設備工事</a></h3>
                <p>あらゆる建築物の電気・設備の設計から施工、そして保守・管理まで行っています。</p>
              </section>
            </li>
            <li>
              <section>
                <p class="img"><a href="<?php echo home_url('/'); ?>works#unit02" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/img02.jpg" width="215" height="130" alt="自動制御設備"></a></p>
                <h3><a href="<?php echo home_url('/'); ?>works#unit02">自動制御設備</a></h3>
                <p>中央監視装置工事、自動制御設備工事、セキュリティー設備工事等を行っています。</p>
              </section>
            </li>
            <li>
              <section>
                <p class="img"><a href="<?php echo home_url('/'); ?>works#unit03" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/img03.jpg" width="215" height="130" alt="冷暖房設備"></a></p>
                <h3><a href="<?php echo home_url('/'); ?>works#unit03">冷暖房設備</a></h3>
                <p>マンション、商業施設等の冷暖房空調など新設工事、改修工事、メンテナンスを行っています。</p>
              </section>
            </li>
            <li>
              <section>
                <p class="img"><a href="<?php echo home_url('/'); ?>works#unit04" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/img04.jpg" width="215" height="130" alt="省エネ・eco"></a></p>
                <h3><a href="<?php echo home_url('/'); ?>works#unit04">省エネ・eco</a></h3>
                <p>LED照明をはじめ、床暖房や業務用エアコンの改善など、省エネに関連した電気設備工事を行っています。 </p>
              </section>
            </li>
            <li>
              <section>
                <p class="img"><a href="<?php echo home_url('/'); ?>works#unit05" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/img05.jpg" width="215" height="130" alt="お水のトラブル"></a></p>
                <h3><a href="<?php echo home_url('/'); ?>works#unit05">お水のトラブル</a></h3>
                <p>当社は所沢市の指定水道工事店です。漏水や詰まりなど、お困りごとはお気軽にご相談ください。</p>
              </section>
            </li>
            <li>
              <section>
                <p class="img"><a href="<?php echo home_url('/'); ?>works#unit06" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/top/img06.jpg" width="215" height="130" alt="お困りごとはありませんか？"></a></p>
                <h3><a href="<?php echo home_url('/'); ?>works#unit06">お困りごとはありませんか？</a></h3>
                <p>IHキッチンの電磁波でお悩みの方や、水質改善に関心のある方へ。</p>
              </section>
            </li>
          </ul>
        </section><!-- /#tproduct -->

        <section id="tnews" class="tsection">
          <h2 class="section_ttl"><img src="assets/img/top/tnews_ttl.gif" width="127" height="45" alt="新着情報"></h2>
          <div class="article_area">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
            <article>
              <dl>
                <dt><?php the_time('Y年m月d日'); ?></dt>
                <dd><h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3></dd>
              </dl>
            </article>
<?php endwhile; endif; ?>
          </div><!-- /.article_area -->
        </section><!-- /#tnews -->
      </main><!-- /#top .main -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>