<?php $this_page_title = single_post_title('', false); ?>
<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <nav id="breadcrumb">
      <ol>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href="<?php echo home_url('/'); ?>"><span itemprop="title">HOME</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href="<?php echo home_url('/'); ?>/news"><span itemprop="title">新着情報一覧</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
          <span itemprop="title"><?php echo $this_page_title; ?></span>
        </li>
      </ol>
    </nav>

    <div id="wrap">
      <main id="news" class="main low">
        <section id="section" class="lsection">
          <h3 class="section_ttl">新着情報</h3>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
          <article class="article">
            <div class="ttl_area">
              <p class="date"><?php the_date('Y年m月d日'); ?></p>
              <h3><?php the_title(); ?></h3>
            </div><!-- /.ttl_area -->
            <div class="txt_area">
              <?php the_content(); ?>
            </div><!-- /.txt_area -->
            <p class="return">&lt;&nbsp;<a href="<?php echo home_url('/'); ?>/news">新着情報一覧に戻る</a></p>
          </article>
<?php endwhile; endif; ?>
        </section><!-- /#section -->
      </main><!-- /#news .main .low -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>