      <aside id="sidebar">
<?php if(( get_post_type() == 'results')): ?>
        <div class="srst">
          <h4>実績・詳細コンテンツ</h4>
          <ul>
            <li><a href="<?php echo home_url('/'); ?>results/electric">・電気設備工事</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/auto">・自動制御設備</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/ac-heater">・冷暖房設備</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/eco">・省エネ・eco</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/water-trouble">・お水のトラブル</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/trouble">・お困りごとはありませんか？</a></li>
          </ul>
        </div><!-- /.srst -->
<?php endif; ?>

        <div class="scnt sbnr">
          <h4><img src="<?php echo home_url('/'); ?>assets/img/scnt_ttl.gif" width="181" height="19" alt="お問い合せ・ご相談"></h4>
          <p class="txt">電気設備等に関するお見積り・ご相談はお気軽にお問い合わせください。</p>
          <p class="tel">tel.<span>04-2903-0963</span></p>
          <p><a href="<?php echo home_url('/'); ?>contact" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/scnt_btn.jpg" width="181" height="42" alt="お問い合わせフォーム"></a></p>
        </div><!-- /.scnt -->
        <p class="sbnr"><a href="<?php echo home_url('/'); ?>company" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/sbnr01.jpg" width="220" height="80" alt="会社について"></a></p>
        <p class="sbnr"><a href="<?php echo home_url('/'); ?>recruit" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/sbnr02.jpg" width="220" height="80" alt="求人について"></a></p>
        <p class="sbnr"><a href="<?php echo home_url('/'); ?>results/eco" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/sbnr03.jpg" width="220" height="127" alt="省エネ工事"></a></p>
        <div class="sblog">
          <p class="sbnr"><a href="<?php echo home_url('/'); ?>blog" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/sbnr04.jpg" width="220" height="88" alt="満石健二の漢塾ぶろぐ"></a></p>
          <p class="old_blog">&gt;&nbsp;<a href="http://ameblo.jp/g-setsubi/" target="_blank">旧ブログ（漢塾）はこちら</a></p>
        </div><!-- /.sblog -->
      </aside><!-- #sidebar -->