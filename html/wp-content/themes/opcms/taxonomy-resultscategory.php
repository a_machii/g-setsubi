<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php $this_page_title = get_term_by('slug',$term,$taxonomy)->name; ?>
<?php
  $term_slug = get_query_var('resultscategory');
  $term_info = get_term_by('slug',$term_slug,'resultscategory');
  $this_term_name = $term_info->name;
  $this_term_slug = $term_info->slug;
  $this_term_id = $term_info->term_id;
?>
<?php get_header(); ?>

    <nav id="breadcrumb">
      <ol>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href="<?php echo home_url('/'); ?>"><span itemprop="title">HOME</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href="<?php echo home_url('/'); ?>results/"><span itemprop="title">実績一覧</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
          <span itemprop="title"><?php echo $this_term_name; ?></span>
        </li>
      </ol>
    </nav>

    <div id="wrap">
      <main id="results" class="main low">
<?php if (is_object_in_term($post->ID, 'resultscategory',array('electric','auto','ac-heater'))): ?>
        <section id="section" class="lsection">
          <h3 class="section_ttl"><?php echo $this_page_title; ?></h3>
          <section class="unit">
            <h4 class="unit_ttl">施工事例（一部抜粋）</h4>
            <ul class="img_area02">
<?php
  $fields = $cfs->get('case-loop');
  foreach ($fields as $field):
?>
<?php
  $attachment_id = $field['case-img'];
  $case_img = wp_get_attachment_image_src( $attachment_id, 'case_img_size' );
?>
              <li><img src="<?php echo $case_img[0]; ?>" alt="施工事例の写真"></li>
<?php endforeach; ?>
            </ul>
          </section><!-- /#unit01 -->

          <section class="unit">
            <h4 class="unit_ttl">施工実績（一部抜粋）</h4>
            <table class="tbl_rst">
<?php
  $fields = $cfs->get('case-loop02');
  foreach ($fields as $field):
?>
              <tr>
                <th><?php echo $field['case-name']; ?></th>
                <td><?php echo $field['case-content']; ?></td>
                <td><?php echo $field['case-date']; ?></td>
              </tr>
<?php endforeach; ?>
            </table>
          </section><!-- /#unit02 -->
        </section><!-- /#section -->

<?php elseif (is_object_in_term($post->ID, 'resultscategory','eco')): ?>
        <section id="section" class="lsection">
          <h3 class="section_ttl">省エネ・eco</h3>
<?php
  $args = array(
    'post_type' => 'results',
    'taxonomy' => 'resultscategory',
    'term' => 'eco',
    'posts_per_page' => 10,
    'paged' => $paged
  );
?>
<?php
  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
    setup_postdata( $post );
?>
          <section class="section_in">
            <h4 class="h4ttl"><?php the_title(); ?></h4>
            <div class="section_body">
              <p class="txt"><?php echo cfs()->get('eco-intro'); ?></p>
              <p class="detail_btn ophv"><a href="<?php the_permalink(); ?>" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/results/detail_btn.jpg" width="155" height="35" alt="詳細はこちら"></a></p>
            </div><!-- /.section_body -->
          </section><!-- /.section_in -->
<?php
  endforeach;
  wp_reset_postdata();
?>

<?php
  if (function_exists("pagination")) {
    pagination($additional_loop->max_num_pages);
  }
?>
        </section><!-- /#section -->

<?php elseif (is_object_in_term($post->ID, 'resultscategory','water-trouble')): ?>
        <section id="wtrb_section" class="lsection">
          <h3 class="section_ttl">お水のトラブル</h3>
          <div class="intro_area">
            <img src="<?php echo home_url('/'); ?>assets/img/results/water-trouble/img04.jpg" width="245" height="257" alt="お水のトラブルの画像" class="left">
            <div class="txt_area01 right">
              <p>当社では、埼玉県所沢市を拠点に関東圏の水漏れ・つまりなど、「水のトラブル」に対応できるサービスをご提供いたしております。</p>
              <ul>
                <li>・トイレの水が止まらない</li>
                <li>・トイレが急に流れなくなった</li>
                <li>・蛇口から水がポタポタ</li>
                <li>・排水口があふれてきた</li>
              </ul>
              <p>など、個人様から法人様まで、水まわりで起きる様々なトラブルや困ったにお応えいたします。水道工事・水道修理・蛇口交換のプロが、即日対応すぐ行きます！ </p>
              <img src="<?php echo home_url('/'); ?>assets/img/results/water-trouble/img08.png" width="430" height="77" alt="当社は所沢市「水道局指定工事店」だから安心です">
            </div><!-- /.txt_area01 -->
          </div><!-- /.intro_area -->
          <section class="unit">
            <h4 class="unit_ttl">水道トラブル</h4>
            <p class="txt">こんなお悩みはありませんか？</p>
            <ul class="trb_list">
              <li><span>●&nbsp;&nbsp;</span>蛇口を絞めても水が漏れる</li>
              <li><span>●&nbsp;&nbsp;</span>パイプの接続部分から水が漏れる</li>
              <li><span>●&nbsp;&nbsp;</span>トイレの水が流れない </li>
              <li><span>●&nbsp;&nbsp;</span>水道の勢いが弱い</li>
              <li><span>●&nbsp;&nbsp;</span>水漏れをしているように見えないのにいつも洗面台の下が湿っている</li>
              <li><span>●&nbsp;&nbsp;</span>トイレや下水管から悪臭がする</li>
              <li><span>●&nbsp;&nbsp;</span>蛇口がぐらぐらしている</li>
            </ul>
            <div class="txt_area02">
              <p>一口に水道トラブルといっても、蛇口のパッキン取り換えから、錆びた水道管の交換、排水管の高圧洗浄など、対処法は多岐に渡ります。</p>
              <p>経験豊富な当社なら、現場の状態を正しく見極め、スピーディに解決することが可能です。修理がいいか、交換がいいか？最善の方法で対処します。ご自身で対処されて、後々トラブルにつながるケースもありますので、不具合を発見した際は、一度当社までご連絡ください。</p>
            </div><!-- /.txt_area02 -->
            <div class="txt_area03">
              <p>さまざまなトラブルに、最善の方法で対処します！</p>
              <p>困ったときは、グリーン設備にご相談ください！</p>
            </div><!-- /.txt_area02 -->

            <section class="box">
              <div class="box_in left">
                <h5>トイレ等配水管のつまり、あふれ</h5>
                <ul>
                  <li>・トイレ周りの修理</li>
                  <li>・便器／タンク交換</li>
                  <li>・フラッシュバルブの修理、交換</li>
                  <li>・床からの水漏れ</li>
                  <li>・パイプ交換</li>
                  <li>・パイプ取り付け、取り外し</li>
                  <li>・排水トラップ取り付け、取り外し</li>
                  <li>・洗濯パン修理、取り替え</li>
                  <li>・食器洗い機の分岐水栓取り付け、取り外し　など</li>
                </ul>
              </div><!-- /.box_in -->
              <p class="img right"><img src="<?php echo home_url('/'); ?>assets/img/results/water-trouble/img05.jpg" width="197" height="132" alt="トイレ等配水管のつまり、あふれの写真" class="right"></p>
            </section><!-- /.box -->

            <section class="box">
              <div class="box_in left">
                <h5>蛇口の水漏れ</h5>
                <ul>
                  <li>・蛇口根元水漏れ</li>
                  <li>・蛇口故障、交換</li>
                  <li>・蛇口水漏れ修理</li>
                  <li>・蛇口移動、新設</li>
                  <li>・シャワーヘッドの故障、水漏れ</li>
                  <li>・レバー、カランの故障、水漏れ</li>
                  <li>・ノズルの故障（水漏れ、水が出ない）</li>
                  <li>・操作部分の故障　など</li>
                </ul>
              </div><!-- /.box_in -->
              <p class="img right"><img src="<?php echo home_url('/'); ?>assets/img/results/water-trouble/img06.jpg" width="197" height="132" alt="蛇口の水漏れの写真" class="right"></p>
            </section><!-- /.box -->

            <section class="box">
              <div class="box_in left">
                <h5>排水・給水工事</h5>
                <ul>
                  <li>・排水管</li>
                  <li>・通気管を更新、更生</li>
                  <li>・給水引込、受水槽、ポンプ設置工事</li>
                  <li>・トイレ・台所等水廻りの給水</li>
                  <li>・排水工事</li>
                  <li>・ボイラー修理、施工　など</li>
                </ul>
              </div><!-- /.box_in -->
              <p class="img right"><img src="<?php echo home_url('/'); ?>assets/img/results/water-trouble/img07.jpg" width="197" height="132" alt="排水・給水工事の写真" class="right"></p>
            </section><!-- /.box -->
          </section><!-- /#unit -->
        </section><!-- /#wtrouble -->

<?php elseif (is_object_in_term($post->ID, 'resultscategory','trouble')): ?>
        <section id="trb_section" class="lsection">
          <h3 class="section_ttl">お困りごとはありませんか？</h3>
<?php
  $args = array(
    'post_type' => 'results',
    'taxonomy' => 'resultscategory',
    'term' => 'trouble',
    'posts_per_page' => 10,
    'paged' => $paged
  );
?>
<?php
  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
    setup_postdata( $post );
?>
          <section class="section_in">
            <h4 class="h4ttl"><?php the_title(); ?></h4>
            <div class="section_body">
              <p class="txt"><?php echo $cfs->get('trouble-intro'); ?></p>
              <p class="detail_btn ophv"><a href="<?php the_permalink(); ?>" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/results/detail_btn.jpg" width="155" height="35" alt="詳細はこちら"></a></p>
            </div><!-- /.section_body -->
          </section><!-- /.section_in -->
<?php
  endforeach;
  wp_reset_postdata();
?>

<?php
  if (function_exists("pagination")) {
    pagination($additional_loop->max_num_pages);
  }
?>
        </section><!-- /#trb_section -->
<?php endif;?>
      </main><!-- /#results .main .low -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>