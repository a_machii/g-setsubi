<?php $this_page_title = get_post_type_object(get_post_type())->label; ?>
<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <nav id="breadcrumb">
      <ol>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href=".<?php echo home_url('/'); ?>"><span itemprop="title">HOME</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
          <span itemprop="title">ブログ</span>
        </li>
      </ol>
    </nav>

    <div id="wrap">
      <main id="blog" class="main low">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
        <article class="article">
          <h3><?php the_title(); ?></h3>
          <p class="date"><?php the_time('Y年m月d日'); ?></p>
          <div class="txt_area">
            <?php the_content(); ?>
          </div>
        </article>
<?php endwhile; endif; ?>

<?php
  if (function_exists("pagination")) {
    pagination($additional_loop->max_num_pages);
  }
?>
      </main><!-- /#news .main .low -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>