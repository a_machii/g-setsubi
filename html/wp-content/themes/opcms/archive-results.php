<?php $this_page_title = get_post_type_object(get_post_type())->label; ?>
<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <nav id="breadcrumb">
      <ol>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href=".<?php echo home_url('/'); ?>"><span itemprop="title">HOME</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
          <span itemprop="title">実績一覧（電気設備工事）</span>
        </li>
      </ol>
    </nav>

    <div id="wrap">
      <main id="results" class="main low">
        <section id="section" class="lsection">
          <h3 class="section_ttl">電気設備工事<span><a href="<?php echo home_url('/'); ?>results/electric/">詳しくはこちら</a></span></h3>
          <h3 class="section_ttl">自動制御設備<span><a href="<?php echo home_url('/'); ?>results/auto/">詳しくはこちら</a></span></h3>
          <h3 class="section_ttl">冷暖房設備<span><a href="<?php echo home_url('/'); ?>results/ac-heater/">詳しくはこちら</a></span></h3>
          <h3 class="section_ttl">省エネ・eco事業<span><a href="<?php echo home_url('/'); ?>results/eco/">詳しくはこちら</a></span></h3>
          <h3 class="section_ttl">お水のトラブル<span><a href="<?php echo home_url('/'); ?>results/water-trouble/">詳しくはこちら</a></span></h3>
          <h3 class="section_ttl">お困りごとはありませんか？<span><a href="<?php echo home_url('/'); ?>results/trouble/">詳しくはこちら</a></span></h3>
        </section><!-- /#section -->
      </main><!-- /#results .main .low -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>