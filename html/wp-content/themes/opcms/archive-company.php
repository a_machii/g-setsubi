<?php $this_page_title = get_post_type_object(get_post_type())->label; ?>
<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <nav id="breadcrumb">
      <ol>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href=".<?php echo home_url('/'); ?>"><span itemprop="title">HOME</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
          <span itemprop="title">会社概要</span>
        </li>
      </ol>
    </nav>

    <div id="wrap">
      <main id="company" class="main low">
        <section id="section01" class="lsection">
          <h3 class="section_ttl">会社情報</h3>
          <table class="tbl">
            <tr>
              <th>商号</th>
              <td><?php echo cfs()->get('company-tbl01'); ?></td>
            </tr>
            <tr>
              <th>住所</th>
              <td><?php echo cfs()->get('company-tbl02'); ?></td>
            </tr>
            <tr>
              <th>資本金</th>
              <td><?php echo cfs()->get('company-tbl03'); ?></td>
            </tr>
            <tr>
              <th>電話</th>
              <td><?php echo cfs()->get('company-tbl04'); ?></td>
            </tr>
            <tr>
              <th>FAX</th>
              <td><?php echo cfs()->get('company-tbl05'); ?></td>
            </tr>
            <tr>
              <th>会社設立年月日</th>
              <td><?php echo cfs()->get('company-tbl06'); ?></td>
            </tr>
            <tr>
              <th>取締役</th>
              <td><?php echo cfs()->get('company-tbl07'); ?></td>
            </tr>
            <tr>
              <th>従業員数</th>
              <td><?php echo cfs()->get('company-tbl12'); ?></td>
            </tr>
            <tr>
              <th>工事内容</th>
              <td><?php echo cfs()->get('company-tbl08'); ?></td>
            </tr>
            <tr>
              <th>許可業種</th>
              <td><?php echo cfs()->get('company-tbl09'); ?></td>
            </tr>
            <tr>
              <th>主要取引先</th>
              <td><?php echo cfs()->get('company-tbl10'); ?></td>
            </tr>
            <tr>
              <th>主要取引銀行</th>
              <td><?php echo cfs()->get('company-tbl11'); ?></td>
            </tr>
          </table>
        </section><!-- /#section01 -->

        <section id="section02" class="lsection">
          <h3 class="section_ttl">アクセス</h3>
          <section class="unit">
            <h4>本社</h4>
            <p class="gmap"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3235.877747197504!2d139.4461953157175!3d35.802934480166066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018ddd8586f7977%3A0x2f341719e2cc8b2c!2z44CSMzU5LTExMTEg5Z-8546J55yM5omA5rKi5biC57eR55S677yT5LiB55uu77yS77yR4oiS77yY!5e0!3m2!1sja!2sjp!4v1475646916238" width="715" height="260" frameborder="0" style="border:0" allowfullscreen></iframe></p>
            <p>所在地：〒359-1111 埼玉県所沢市緑町3-21-8</p>
          </section><!-- /.unit -->

          <section class="unit">
            <h4>山梨営業所</h4>
            <p class="gmap"></p>
            <p>所在地：〒409-1502 山梨県北杜市大泉町谷戸字並木上8950-44</p>
          </section><!-- /.unit -->
        </section><!-- /#section02 -->
      </main><!-- /#company .main .low -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>