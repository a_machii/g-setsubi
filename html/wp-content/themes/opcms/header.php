<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php
  global $this_page_title;
  global $this_page_keywd;
  global $this_page_desc;
  global $this_page_slug;
?>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php if($this_page_title) : echo $this_page_title. ' | '; endif; ?>電気設備工事、電気工事、計装工事のグリーン設備（新着情報・会社案内）（埼玉県所沢市）</title>
    <meta name="description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_page_title) : echo $this_page_title. ' - '; endif; ?>電気設備工事、電気工事、計装工事のグリーン設備です。冷暖房設備や太陽光発電、オール電化なども取り扱っております。">
    <meta name="keywords" content="<?php if($this_page_keywd) : echo $this_page_keywd. ','; elseif($this_page_title) : echo $this_page_title. ','; endif; ?>電気設備,自動制御設備,冷暖房設備,電気工事,計装工事,太陽光発電,オール電化" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no,address=no,email=no">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=980, maximum-scale=1.0, user-scalable=yes">
    <meta property="og:title" content="<?php if($this_page_title) : echo $this_page_title. ' | '; endif; ?>電気設備工事、電気工事、計装工事のグリーン設備（新着情報・会社案内）（埼玉県所沢市）"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo home_url('/'); ?><?php if($this_page_slug) : echo $this_page_slug; endif; ?>" />
    <meta property="og:site_name"  content="株式会社　グリーン設備" />
    <meta property="og:description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_page_title) : echo $this_page_title. ' - '; endif; ?>電気設備工事、電気工事、計装工事のグリーン設備です。冷暖房設備や太陽光発電、オール電化なども取り扱っております。" />
    <link rel="stylesheet" href="<?php echo home_url('/'); ?>assets/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!--&#91;if lt IE 9&#93;>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <!&#91;endif&#93;-->
<?php wp_head(); ?>
  </head>

  <body>
    <header id="header">
      <div class="inner">
        <h1>埼玉県所沢市の電気工事、設備工事、計装工事のグリーン設備 </h1>
        <p class="hlogo left"><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>assets/img/hlogo.gif" width="253" height="35" alt="株式会社グリーン設備"></a></p>
        <div class="unit left">
          <p class="subnav01 left"><a href="<?php echo home_url('/'); ?>company" class="ophv">&gt;&nbsp;会社概要</a></p>
          <p class="subnav02 right"><a href="<?php echo home_url('/'); ?>recruit" class="ophv">&gt;&nbsp;求人案内</a></p>
        </div><!-- /.unit -->
        <p class="hcnt_btn"><a href="<?php echo home_url('/'); ?>contact" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/hcnt_btn.gif" width="279" height="71" alt="見積もり・問い合わせ"></a></p>

        <nav id="gnav">
          <ul>
            <li><a href="<?php echo home_url('/'); ?>" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav01_n.gif" width="163" height="35" alt="HOME"></a></li>
            <li><a href="<?php echo home_url('/'); ?>works" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav02_n.gif" width="163" height="35" alt="事業内容"></a></li>
            <li><a href="<?php echo home_url('/'); ?>news" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav03_n.gif" width="163" height="35" alt="新着情報"></a></li>
            <li><a href="<?php echo home_url('/'); ?>results" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav04_n.gif" width="163" height="35" alt="実績一覧"></a></li>
            <li><a href="<?php echo home_url('/'); ?>results/water-trouble" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav05_n.gif" width="163" height="35" alt="お水のトラブル"></a></li>
            <li><a href="<?php echo home_url('/'); ?>results/eco" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/gnav06_n.gif" width="162" height="35" alt="省エネ・eco事業"></a></li>
          </ul>
        </nav>
      </div><!-- /.inner -->
    </header>

<?php if(is_home()): ?>
    <div id="mimg">
      <div class="inner">
        <img src="<?php echo home_url('/'); ?>assets/img/top/mimg.jpg" width="980" height="350" alt="株式会社グリーン設備のメイン画像">
      </div><!-- /.inner -->
    </div><!-- /#mimg -->
<?php elseif(is_post_type_archive('blog')): ?>
    <div id="blog_mimg">
      <div class="inner">
        <h2 class="page_ttl"><img src="<?php echo home_url('/'); ?>assets/img/blog/mimg.png" width="367" height="120" alt="満石健二の漢塾ぶろぐ"></h2>
      </div><!-- /.inner -->
    </div><!-- /#blog_mimg -->
<?php else: ?>
    <div id="low_mimg">
      <div class="inner">
        <h2 class="page_ttl"><img src="<?php echo home_url('/'); ?>assets/img/<?php echo $this_page_slug; ?>/page_ttl.png" alt="<?php echo $this_page_title; ?>"></h2>
      </div><!-- /.inner -->
    </div><!-- /#low_mimg -->
<?php endif; ?>