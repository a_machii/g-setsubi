      <div id="to_top"><a href="#header" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/totop_btn.gif" width="72" height="13" alt="ページトップへ"></a></div>
    </div><!-- /#wrap -->

    <footer id="footer">
      <div class="inner">
        <p class="txt">電気設備工事、電気工事、計装工事なら埼玉県所沢市【株式会社グリーン設備】まで！　「技術」と「まごころ」を持って法人企業様、個人様のベストパートナーとしていつも全力で電気設備工事をトータルサポートいたします。 </p>
        <nav id="fnav" class="left">
          <ul class="left">
            <li><a href="<?php echo home_url('/'); ?>">ホーム</a></li>
            <li><a href="<?php echo home_url('/'); ?>works">事業内容</a></li>
            <li><a href="<?php echo home_url('/'); ?>news">新着情報</a></li>
            <li><a href="<?php echo home_url('/'); ?>results">実績一覧</a></li>
            <li><a href="<?php echo home_url('/'); ?>company/">会社概要</a></li>
            <li><a href="<?php echo home_url('/'); ?>recruit/">求人案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>contact">お問い合せ</a></li>
          </ul>

          <ul class="right">
            <li><a href="<?php echo home_url('/'); ?>results/electric">電気自動車設備工事</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/auto">自動制御設備</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/ac-heater">冷暖房設備</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/eco">省エネ・eco</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/water-trouble">お水のトラブル</a></li>
            <li><a href="<?php echo home_url('/'); ?>results/trouble">お困りのことはありませんか？</a></li>
          </ul>
        </nav>

        <div class="unit right">
          <dl>
            <dt><img src="<?php echo home_url('/'); ?>assets/img/flogo.gif" width="262" height="35" alt="株式会社グリーン設備"></dt>
            <dd>〒359-1111埼玉県所沢市緑町3-21-8</dd>
            <dd>電話：04-2903-0963（本社代表）</dd>
            <dd>FAX：04-2903-0968</dd>
          </dl>
          <p class="fcnt_btn"><a href="<?php echo home_url('/'); ?>contact" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/fcnt_btn.jpg" width="225" height="35" alt="お問い合わせフォーム"></a></p>
        </div><!-- /.unit -->
        <p class="cr">Copyright (c) グリーン設備 All Rights Reserved.</p>
      </div><!-- /.finner -->
    </footer><!-- /#footer -->

    <!-- script -->
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets/js/rollover/rollover.js"></script>
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets/js/rollover/opacity-rollover2.1.js"></script>
    <script  type="text/javascript" src="<?php echo home_url('/'); ?>assets/js/smoothScrollEx.js"></script>

    <!--ロールオーバーの記述-->
    <script type="text/javascript">
      $(document).ready(function() {
        $('.ophv').opOver(1.0, 0.6, 200, 200);
      });
    </script>
<?php wp_footer(); //</body>の直前に記述する ?>
  </body>
</html>