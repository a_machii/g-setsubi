<?php $this_page_title = get_post_type_object(get_post_type())->label; ?>
<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <nav id="breadcrumb">
      <ol>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href=".<?php echo home_url('/'); ?>"><span itemprop="title">HOME</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
          <span itemprop="title">求人案内</span>
        </li>
      </ol>
    </nav>

    <div id="wrap">
      <main id="recruit" class="main low">
        <section id="section01" class="lsection">
          <h3 class="section_ttl">採用方針</h3>
          <div class="txt_area01">
            <p class="txt">明るくてやる気のある人、</p>
            <p>若くて元気のある人を募集しています！</p>
          </div><!-- /.txt_area01 -->
          <div class="txt_area02">
            <p>みなさま、はじめまして。 </p>
            <p>会社名の由来は、会社の場所が所沢市緑町ということもありますが、緑色は安全を意味すると考え「グリーン設備」と名づけました。おかげさまで今まで事故一つなく色々なお仕事をさせていただいております。</p>
            <p>作業中は安全を第一にだらだらぜずにキビキビと行っていますが、基本的に楽しくてにぎやかなのが好きなので、仕事以外ではアットホームな和気あいあいとした会社です。（仕事も遊びもとことんまで！中途半端は嫌い。）</p>
            <p>さて、私自身新しいことにどんどん、チャレンジしていくのが好きなのですが、最近は若くて元気の良い新人を一からじっくりと育てて行きたいと思うようになってきました。</p>
            <p>今の自分に満足していてはいけない！過去を振り返らず、常に前進あるのみ！」と思っております。あかるくて、本気で、手に職をつけたいと思っている、若くて、元気な方を、お待ちしています。</p>
            <p>まずはお気軽にお問い合わせください。</p>
          </div><!-- /.txt_area02 -->
          <p class="signature"><img src="<?php echo home_url('/'); ?>assets/img/recruit/signature.jpg" width="193" height="59" alt="株式会社　グリーン設備　代表取締役　満石健二" class="right"></p>
        </section><!-- /#section01 -->

        <section id="section02" class="lsection">
          <h3 class="section_ttl">募集要項</h3>
          <table class="tbl">
            <tr>
              <th>募集職種</th>
              <td><?php echo cfs()->get('recruit-tbl01') ?></td>
            </tr>
            <tr>
              <th>仕事内容</th>
              <td><?php echo cfs()->get('recruit-tbl02') ?></td>
            </tr>
            <tr>
              <th>応募資格</th>
              <td><?php echo cfs()->get('recruit-tbl03') ?></td>
            </tr>
            <tr>
              <th>雇用形態</th>
              <td><?php echo cfs()->get('recruit-tbl04') ?></td>
            </tr>
            <tr>
              <th>勤務地</th>
              <td><?php echo cfs()->get('recruit-tbl05') ?></td>
            </tr>
            <tr>
              <th>交通</th>
              <td><?php echo cfs()->get('recruit-tbl06') ?></td>
            </tr>
            <tr>
              <th>勤務時間</th>
              <td><?php echo cfs()->get('recruit-tbl07') ?></td>
            </tr>
            <tr>
              <th>給与</th>
              <td><?php echo cfs()->get('recruit-tbl08') ?></td>
            </tr>
            <tr>
              <th>待遇</th>
              <td><?php echo cfs()->get('recruit-tbl09') ?></td>
            </tr>
            <tr>
              <th>休日・休暇</th>
              <td><?php echo cfs()->get('recruit-tbl10') ?></td>
            </tr>
            <tr>
              <th>その他</th>
              <td><?php echo cfs()->get('recruit-tbl11') ?></td>
            </tr>
          </table>
          <p class="cnt_btn"><a href="<?php echo home_url('/'); ?>contact" class="ophv">求人に関するお問い合わせ応募はこちら&nbsp;&nbsp;&gt;</a></p>
        </section><!-- /#section02 -->
      </main><!-- /#recruit .main .low -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>