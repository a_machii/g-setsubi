<?php $this_page_title = get_post_type_object(get_post_type())->label; ?>
<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <nav id="breadcrumb">
      <ol>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href=".<?php echo home_url('/'); ?>"><span itemprop="title">HOME</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
          <span itemprop="title">新着情報一覧</span>
        </li>
      </ol>
    </nav>

    <div id="wrap">
      <main id="news" class="main low">
        <section id="section" class="lsection">
          <h3 class="section_ttl">新着情報一覧</h3>
          <div class="article_area">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
            <article>
              <dl>
                <dt><?php the_time('Y年m月d日'); ?></dt>
                <dd><h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3></dd>
              </dl>
            </article>
<?php endwhile; endif; ?>
          </div><!-- /.article_area -->
        </section><!-- /#section -->
      </main><!-- /#news .main .low -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>