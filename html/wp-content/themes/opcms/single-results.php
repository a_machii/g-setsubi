<?php $this_page_title = single_post_title('', false); ?>
<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php
  $term_slug = get_query_var('resultscategory');
  $term_info = get_term_by('slug',$term_slug,'resultscategory');
  $this_term_name = $term_info->name;
  $this_term_slug = $term_info->slug;
  $this_term_id = $term_info->term_id;
?>
<?php get_header(); ?>

    <nav id="breadcrumb">
      <ol>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href="<?php echo home_url('/'); ?>"><span itemprop="title">HOME</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href="<?php echo home_url('/'); ?>results/"><span itemprop="title">実績一覧</span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a itemprop="url" href="<?php echo home_url('/'); ?>results/<?php echo $this_term_slug; ?>/"><span itemprop="title"><?php echo $this_term_name; ?></span></a>&nbsp;&gt;&nbsp;
        </li>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
          <span itemprop="title"><?php echo $this_page_title; ?></span>
        </li>
      </ol>
    </nav>

    <div id="wrap">
      <main id="results" class="main low">
<?php if (is_object_in_term($post->ID, 'resultscategory','eco')): ?>
        <section id="section" class="lsection">
          <h3 class="section_ttl">省エネ・eco</h3>
          <section class="unit">
            <h4 class="unit_ttl"><?php echo $this_page_title; ?></h4>
            <p class="cp"><?php echo cfs()->get('eco-cp'); ?></p>
            <div class="txt_area">
<?php echo cfs()->get('eco-text'); ?>
            </div><!-- /.txt_area -->
            <div class="catalog">
              <p>●<?php echo $this_page_title; ?>カタログ</p>
              <p><a href="<?php echo cfs()->get('eco-catalog'); ?>" target="_blank" class="ophv"><img src="<?php echo home_url('/'); ?>assets/img/results/eco/dl_btn.gif" width="169" height="24" alt="ダウンロード"></a><!--&nbsp;<span>（PDFカタログファイルサイズ：32.9MB）</span>--></p>
            </div><!-- /.catalog -->
            <div class="img_area">
              <p>●<?php echo $this_page_title; ?>設置例</p>
              <ul>
<?php
  $fields = $cfs->get('eco-loop');
  foreach ($fields as $field):
?>
<?php
  $attachment_id = $field['eco-img'];
  $eco_img = wp_get_attachment_image_src( $attachment_id, 'ecoimg_size');
?>
                <li><img src="<?php echo $eco_img[0]; ?>" width="230" height="173 "alt="<?php echo $this_page_title; ?>例の写真"></li>
<?php endforeach; ?>
              </ul>
            </div><!-- /.img_area -->
          </section><!-- /.unit -->
        </section><!-- /#section -->

<?php elseif (is_object_in_term($post->ID, 'resultscategory','trouble')): ?>
        <section id="trb_section" class="lsection">
          <h3 class="section_ttl">お困りごとはありませんか？</h3>
          <section class="unit">
            <h4 class="unit_ttl"><?php echo $this_page_title; ?></h4>
            <p class="txt01"><?php echo cfs()->get('trouble-cp'); ?></p>
<?php echo cfs()->get('trouble-text'); ?>
          </section><!-- /.unit -->
        </section><!-- /#trouble -->
<?php endif;?>
      </main><!-- /#results .main .low -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>