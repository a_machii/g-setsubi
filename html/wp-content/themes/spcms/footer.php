    <footer id="footer">
      <nav>
        <ul class="fnav">
          <li><a href="<?php echo home_url('/'); ?>">HOME</a></li><!--
          --><li><a href="<?php echo home_url('/'); ?>works">事業内容</a></li><!--
          --><li><a href="<?php echo home_url('/'); ?>company">会社案内</a></li><!--
          --><li><a href="<?php echo home_url('/'); ?>blog">ブログ</a></li><!--
          --><li><a href="<?php echo home_url('/'); ?>recruit">求人案内</a></li><!--
          --><li><a href="<?php echo home_url('/'); ?>contact">お問い合わせ</a></li>
        </ul>
      </nav>

      <div class="address">
        <div class="inner">
          <p>株式会社グリーン設備</p>
          <p>〒359-1111埼玉県所沢市緑町3-21-8</p>
          <p>電話：04-2903-0963（本社代表）／FAX：04-2903-0968</p>
        </div><!-- /.inner -->
      </div><!-- /.address -->

      <div class="bottom_area">
        <ul class="switch">
          <li><a href="?pc-switcher=1" id="btn_pc"><span>PC</span></a></li>
          <li><a href="?pc-switcher=0" id="btn_sp"><span>モバイル</span></a></li>
        </ul>
        <p class="cc">Copyright © Koa/acacia. All Rights Reserved.</p>
      </div><!-- /.bottom_area -->
    </footer>

    <!-- script -->
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets-sp/js/rollover/rollover.js"></script>
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets-sp/js/rollover/opacity-rollover2.1.js"></script>
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets-sp/js/smoothScrollEx.js"></script>
    <script type="text/javascript" src="<?php echo home_url('/'); ?>assets-sp/js/jquery.cookie.js"></script>

    <!-- ロールオーバー -->
    <script>
      $(document).ready(function() {
        $('.ophv').opOver(1.0, 0.6, 200, 200);
      });
    </script>

    <!-- 電話・メニューボタン -->
    <script>
      $(document).ready(function(){
        $(window).on('load resize',function(){
          var w = $(window).innerWidth();
          var h = $('#header').innerHeight();
          var tel_btn_w = w * 0.0472;
          var menu_btn_w = w * 0.0638;
          var btn_ptb = w * 0.0513;
          var tel_btn_plr = w * 0.0444;
          var menu_btn_plr = w * 0.0375;
          var menu_lia_ptb = h / 4;
          $('#tel_btn').css({
            'padding-top': btn_ptb,
            'padding-right': tel_btn_plr,
            'padding-bottom': btn_ptb,
            'padding-left': tel_btn_plr,
          });
          $('#tel_btn img').css({
            width: tel_btn_w,
          });
          $('#menu_btn').css({
            'padding-top': btn_ptb,
            'padding-right': menu_btn_plr,
            'padding-bottom': btn_ptb,
            'padding-left': menu_btn_plr,
          });
          $('#menu_btn img').css({
            width: menu_btn_w,
          });
          $('#menu').css({
            top: h
          });
          $('#menu li a').css({
            'padding-top': menu_lia_ptb,
            'padding-bottom': menu_lia_ptb
          });
        });
      });
    </script>

    <!-- アコーディオンメニュー -->
    <script>
      $(document).ready(function(){
        $(window).resize(function() {
          var w = $(window).width();
          if(720 <= w){ //ここにブレイクポイントの数値を入れて条件分岐
            $('#menu').css("display","none");
          }
        });
        $('#menu_btn').on('click',function(){
          $('#menu').slideToggle();
        });
      });
    </script>

<?php if(is_home()): ?>
    <!-- 新着情報 -->
    <script>
      $(document).ready(function(){
        $(window).resize('load resize',function(){
          var t_news_w = $('#t_news').innerWidth();
          var article_area_h = t_news_w * 0.54;
          $('.article_area').css({
            height: article_area_h,
          });
        });
      });
    </script>
<?php endif; ?>

<?php if(is_page('works')): ?>
    <!-- 事業内容アコーディオン -->
    <script>
      $(function(){
        $(".accordion h3").click(function(){
          $(this).next(".unit").slideToggle();
          $(this).children("span").toggleClass("open");
        });
      });
    </script>
<?php endif; ?>

<?php wp_footer(); ?>
  </body>
</html>