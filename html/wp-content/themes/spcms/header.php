<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php
  global $this_page_title;
  global $this_page_keywd;
  global $this_page_desc;
  global $this_page_slug;
?>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php if($this_page_title) : echo $this_page_title. ' | '; endif; ?>電気設備工事、電気工事、計装工事のグリーン設備（新着情報・会社案内）（埼玉県所沢市）</title>
    <meta name="description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_page_title) : echo $this_page_title. ' - '; endif; ?>電気設備工事、電気工事、計装工事のグリーン設備です。冷暖房設備や太陽光発電、オール電化なども取り扱っております。" >
    <meta name="keywords" content="<?php if($this_page_keywd) : echo $this_page_keywd. ','; elseif($this_page_title) : echo $this_page_title. ','; endif; ?>電気設備,自動制御設備,冷暖房設備,電気工事,計装工事,太陽光発電,オール電化" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no,address=no,email=no">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta property="og:title" content="<?php if($this_page_title) : echo $this_page_title. ' | '; endif; ?>電気設備工事、電気工事、計装工事のグリーン設備（新着情報・会社案内）（埼玉県所沢市）"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo home_url('/'); ?><?php if($this_page_slug) : echo $this_page_slug; endif; ?>" />
    <meta property="og:site_name"  content="株式会社 グリーン設備" />
    <meta property="og:description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_page_title) : echo $this_page_title. ' - '; endif; ?>電気設備工事、電気工事、計装工事のグリーン設備です。冷暖房設備や太陽光発電、オール電化なども取り扱っております。" />
    <link rel="stylesheet" type="text/css" href="<?php echo home_url('/'); ?>assets-sp/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!--&#91;if lt IE 9&#93;>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <!&#91;endif&#93;-->
<?php wp_head(); ?>
  </head>

  <body>
    <header>
      <div id="header">
        <h1><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>assets-sp/img/logo.gif" width="351" height="39" alt="株式会社グリーン設備"></a></h1>
        <div class="unit">
          <p id="tel_btn"><a href="tel:04-2903-0963"><img src="<?php echo home_url('/'); ?>assets-sp/img/tel_btn.gif" width="34" height="36" alt="電話:04-2903-0963"></a></p>
          <p id="menu_btn"><img src="<?php echo home_url('/'); ?>assets-sp/img/menu_btn.gif" width="46" height="36" alt="メニューボタン"></p>
        </div><!-- /.unit -->
      </div><!-- /#header -->

      <nav>
        <ul id="menu">
          <li><a href="<?php echo home_url('/'); ?>">HOME</a></li>
          <li><a href="<?php echo home_url('/'); ?>works">事業内容</a></li>
          <li><a href="<?php echo home_url('/'); ?>company">会社案内</a></li>
          <li><a href="<?php echo home_url('/'); ?>blog">ブログ</a></li>
          <li><a href="<?php echo home_url('/'); ?>recruit">求人案内</a></li>
          <li><a href="<?php echo home_url('/'); ?>contact">お問い合わせ</a></li>
        </ul>
      </nav>

<?php if(is_home()): ?>
      <div id="mimg">
        <img src="<?php echo home_url('/'); ?>assets-sp/img/top/mimg.jpg" width="720" height="375" alt="株式会社グリーン設備のメイン画像">
      </div><!-- /#mimg -->
<?php elseif(is_post_type_archive('blog')): ?>
      <div id="blog_mimg">
        <h2 class="blog_mimg"><img src="<?php echo home_url('/'); ?>assets-sp/img/blog/mimg.gif" width="420" height="138" alt="満石健二の漢塾ブログ"></h2>
      </div><!-- /#mimg -->
<?php else: ?>
      <div id="mimg">
        <h2><img src="<?php echo home_url('/'); ?>assets-sp/img/<?php echo $this_page_slug ?>/mimg.jpg" width="720" height="213" alt="<?php echo $this_page_title ?>"></h2>
      </div><!-- /#mimg -->
<?php endif; ?>
    </header>