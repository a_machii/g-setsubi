<?php /* Template Name: page */ ?>
<?php
  $page_id = $post->ID; //xxxに 固定ページIDを入力
  $content = get_page($page_id);
  $this_page_title = $content->post_title;
  $this_page_slug = $content->post_name;
?>
<?php get_header(); ?>

    <main id="contact" class="low main">
      <div class="inner">
        <section id="section01" class="l_section">
          <h3 class="h3ttl_a"><img src="<?php echo home_url('/'); ?>assets-sp/img/thanks/h3ttl01.gif" width="680" height="65" alt="お問い合わせありがとうございました"></h3>
          <div class="txt_area">
            <p>お問い合わせありがとうございました。</p>
            <p>折り返しご返信いたします。２，３日過ぎても返信がない場合は、お手数ですが下記までお電話にてご連絡いただきますよう、お願いいたします。</p>
          </div><!-- /.txt_area -->
          <dl>
          <div class="txt_area02">
            <p>●電話番号：03-3903-0963</p>
            <p>●受付時間：平日10：00～18：00</p>
            <p>（土・日・祝日および弊社指定休業日を除きます）</p>
          </div><!-- /.txt_area02 -->
          <p class="tel_btn"><a href="tel:0339030963" class="ophv"><img src="<?php echo home_url('/'); ?>assets-sp/img/contact/tel_btn.gif" width="555" height="74" alt="こちらをタップして電話をかける"></a></p>
        </section><!-- /#section01 -->
      </div><!-- /.inner -->
      <p class="to_top"><a href="#header">PAGE TOP</a></p>
    </main><!-- /#contact -->

<?php get_footer(); ?>