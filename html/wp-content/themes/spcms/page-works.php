<?php /* Template Name: page */ ?>
<?php
  $page_id = $post->ID; //xxxに 固定ページIDを入力
  $content = get_page($page_id);
  $this_page_title = $content->post_title;
  $this_page_slug = $content->post_name;
?>
<?php get_header(); ?>

    <main id="works" class="low main">
      <div class="inner">
        <p class="txt01">グリーン設備では、薬品会社や食品会社等の製品を製造する工場内の自動制御設備工事（プラント計装工事）、高層ビル等の換気、冷暖房を効率よく行うための自動制御設備（空調計装工事）など、主に施設の自動制御設備に関する電気設備工事を中心に施工を承っております。</p>
        <ul class="accordion">
          <li>
            <h3><span><img src="<?php echo home_url('/'); ?>assets-sp/img/works/h3ttl01.gif" width="680" height="103" alt="電気・設備工事"></span></h3>
            <div class="unit">
              <div class="box">
                <p class="img"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/img01.jpg" width="310" height="219" alt="電気・設備工事の写真"></p>
                <p>住宅、ビル、店舗、事務所等の電気・設備を施工します。打合せ、設計、施工、メンテナンスまで一元管理で行います。</p>
                <p>また、ＩＴの情報通信関連工事など、お客様のご要望に応じて、様々な電気設備の設計・施工をご提供させていただいております。</p>
              </div><!-- /.box -->
              <h4>●営業種目</h4>
              <p class="txt02">受変電設備、幹線・動力設備、電灯・動力設備工事、ＴＶ協調設備空調設備、防災・消防設備、通信LAN設備、太陽光発電設備など</p>
              <p class="detail_btn"><a href="<?php echo home_url('/'); ?><?php echo home_url('/'); ?>results/electric"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/detail_btn01.png" width="306" height="62" alt="成功事例はこちら"></a></p>
            </div><!-- /.unit -->
          </li>

          <li>
            <h3><span><img src="<?php echo home_url('/'); ?>assets-sp/img/works/h3ttl02.gif" width="680" height="103" alt="自動制御設備"></span></h3>
            <div class="unit">
              <div class="box">
                <p class="img"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/img02.jpg" width="310" height="219" alt="自動制御設備の写真"></p>
                <p>室内の温度、湿度などの温熱環境からCO2、塵埃、照明まで、建物の利用者や生産設備などにとって最適な状態にコントロールするシステムです。</p>
                <p>自動制御機器の作動確認や経年劣化診断などを行い、システムを長持ちさせるよう努力いたします。また、異常発生時の緊急対応も素早く行います。</p>
              </div><!-- /.box -->
              <h4>●営業種目</h4>
              <p class="txt02">中央監視装置工事、自動制御設備工事、セキュリティー設備工事など</p>
              <p class="detail_btn"><a href="<?php echo home_url('/'); ?><?php echo home_url('/'); ?>results/auto"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/detail_btn01.png" width="306" height="62" alt="施工事例はこちら"></a></p>
            </div><!-- /.unit -->
          </li>

          <li>
            <h3><span><img src="<?php echo home_url('/'); ?>assets-sp/img/works/h3ttl03.gif" width="680" height="103" alt="冷暖房設備"></span></h3>
            <div class="unit">
              <div class="box">
                <p class="img"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/img03.jpg" width="310" height="219" alt="冷暖房設備の写真"></p>
                <p>快適な生活を送るために必要な空調・冷暖房設備の工事を行っています。</p>
                <p>個人のお客様から法人のお客様まで、幅広く施工・メンテナンス、アフターサービスまで一貫したサービスを提供させていただいております。</p>
              </div><!-- /.box -->
              <h4>●営業種目</h4>
              <p class="txt02">家庭用・業務用空調設備、給湯設備、恒温恒湿設備、各種低温装置、業務用冷凍冷蔵設備、各種ボイラー工事など</p>
              <p class="detail_btn"><a href="<?php echo home_url('/'); ?><?php echo home_url('/'); ?>results/ac-heater"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/detail_btn01.png" width="306" height="62" alt="施工事例はこちら"></a></p>
            </div><!-- /.unit -->
          </li>

          <li>
            <h3><span><img src="<?php echo home_url('/'); ?>assets-sp/img/works/h3ttl04.gif" width="680" height="103" alt="省エネ・eco"></span></h3>
            <div class="unit">
              <div class="box">
                <p class="img"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/img04.jpg" width="310" height="219" alt="省エネ・ecoの写真"></p>
                <p>LED照明をはじめ、床暖房や業務用エアコンの改善など、省エネに関連した電気設備工事を行っています。</p>
                <p>省エネのコンサルタントとして、空調設備はもちろんのこと、LED照明設備、変電設備、給湯設備、床暖房設備などメーカーを問わず、今後も幅広い事業のご提案いたします。</p>
              </div><!-- /.box -->
              <p class="detail_btn"><a href="<?php echo home_url('/'); ?><?php echo home_url('/'); ?>results/eco"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/detail_btn02.png" width="306" height="62" alt="各種詳細はこちら"></a></p>
            </div><!-- /.unit -->
          </li>

          <li>
            <h3><span><img src="<?php echo home_url('/'); ?>assets-sp/img/works/h3ttl05.gif" width="680" height="103" alt="お水のトラブル"></span></h3>
            <div class="unit">
              <div class="box">
                <p class="img"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/img05.jpg" width="310" height="219" alt="お水のトラブルの写真"></p>
                <p>一口に水のトラブルといっても、蛇口のパッキン取り換えから、錆びた水道管の交換、排水管の高圧洗浄など、対処法は多岐に渡ります。</p>
                <p>経験豊富な当社なら、現場の状態を正しく見極め、スピーディに解決することが可能です。修理がいいか、交換がいいか？最善の方法で対処します。ご自身で対処されて、後々トラブルにつながるケースもありますので、不具合を発見した際は、一度当社までご連絡ください。</p>
              </div><!-- /.box -->
              <p class="img02"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/img07.png" width="680" height="193" alt="指定工事店の画像"></p>
              <p class="detail_btn"><a href="<?php echo home_url('/'); ?><?php echo home_url('/'); ?>results/water-trouble"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/detail_btn02.png" width="306" height="62" alt="各種詳細はこちら"></a></p>
            </div><!-- /.unit -->
          </li>

          <li>
            <h3><span><img src="<?php echo home_url('/'); ?>assets-sp/img/works/h3ttl06.gif" width="680" height="103" alt="お困りごとはありませんか？"></span></h3>
            <div class="unit">
              <div class="box">
                <p class="img"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/img06.jpg" width="310" height="219" alt="お水のトラブルの写真"></p>
                <p>IHキッチンの電磁波や、水質改善についてなど、話を聞いたことはあるけれど、内容がよくわからなくて漠然とした不安がある。興味がある等、関心のある方へ。</p>
                <p>何かお困りごとがありましたら、当社までお気軽にお問い合わせください。</p>
              </div><!-- /.box -->
              <p class="detail_btn"><a href="<?php echo home_url('/'); ?><?php echo home_url('/'); ?>results/trouble"><img src="<?php echo home_url('/'); ?>assets-sp/img/works/detail_btn02.png" width="306" height="62" alt="各種詳細はこちら"></a></p>
            </div><!-- /.unit -->
          </li>
        </ul>
      </div><!-- /.inner -->
      <p class="to_top"><a href="#header">PAGE TOP</a></p>
    </main><!-- /#top -->

<?php get_footer(); ?>