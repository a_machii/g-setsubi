<?php $this_page_title = get_post_type_object(get_post_type())->label; ?>
<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <main id="blog" class="low main">
      <div class="inner">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
        <article class="article">
          <h3><?php the_title(); ?></h3>
          <p class="date"><?php the_time('Y年m月d日') ?></p>
          <div class="txt_area">
            <?php the_content(); ?>
          </div>
        </article><!-- /.article -->
<?php endwhile; endif; ?>
      </div><!-- /.inner -->
      <p class="to_top"><a href="#header">PAGE TOP</a></p>
    </main><!-- /#blog -->

<?php get_footer(); ?>