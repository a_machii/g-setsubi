<?php $this_page_title = get_post_type_object(get_post_type())->label; ?>
<?php $this_page_slug = get_post_type_object(get_post_type())->name; ?>
<?php get_header(); ?>

    <main id="recruit" class="low main">
      <div class="inner">
        <section id="section01" class="l_section">
          <h3 class="h3ttl_b"><img src="<?php echo home_url('/'); ?>assets-sp/img/recruit/h3ttl01.gif" width="680" height="65" alt="採用方針"></h3>
          <p class="cp"><img src="<?php echo home_url('/'); ?>assets-sp/img/recruit/catch_copy.gif" width="563" height="73" alt="明るくてやる気のある人、若くて元気のある人を募集しています！"></p>
          <div class="txt_area">
            <p>みなさま、はじめまして。</p>
            <p>会社名の由来は、会社の場所が所沢市緑町ということもありますが、緑色は安全を意味すると考え「グリーン設備」と名づけました。おかげさまで今まで事故一つなく色々なお仕事をさせていただいております。</p>
            <p>作業中は安全を第一にだらだらぜずにキビキビと行っていますが、基本的に楽しくてにぎやかなのが好きなので、仕事以外ではアットホームな和気あいあいとした会社です。（仕事も遊びもとことんまで！中途半端は嫌い。）</p>
            <p>さて、私自身新しいことにどんどん、チャレンジしていくのが好きなのですが、最近は若くて元気の良い新人を一からじっくりと育てて行きたいと思うようになってきました。</p>
            <p>「今の自分に満足していてはいけない！過去を振り返らず、常に前進あるのみ！」と思っております。あかるくて、本気で、手に職をつけたいと思っている、若くて、元気な方を、お待ちしています。</p>
            <p>まずはお気軽にお問い合わせください。</p>
          </div><!-- /.txt_area -->
          <p class="signature"><img src="<?php echo home_url('/'); ?>assets-sp/img/recruit/signature.gif" width="226" height="69" alt="株式会社グリーン設備　代表取締役　満石健二"></p>
        </section><!-- /#section01 -->

        <section id="section02" class="l_section">
          <h3 class="h3ttl_a"><img src="<?php echo home_url('/'); ?>assets-sp/img/recruit/h3ttl02.gif" width="680" height="65" alt="募集要項"></h3>
          <div class="tbl_wrap">
            <table class="rec_tbl">
              <tr>
                <th>募集職種</th>
                <td><?php echo cfs()->get('recruit-tbl01') ?></td>
              </tr>
              <tr>
                <th>仕事内容</th>
                <td><?php echo cfs()->get('recruit-tbl02') ?></td>
              </tr>
              <tr>
                <th>応募資格</th>
                <td><?php echo cfs()->get('recruit-tbl03') ?></td>
              </tr>
              <tr>
                <th>雇用形態</th>
                <td><?php echo cfs()->get('recruit-tbl04') ?></td>
              </tr>
              <tr>
                <th>勤務地</th>
                <td><?php echo cfs()->get('recruit-tbl05') ?></td>
              </tr>
              <tr>
                <th>交通</th>
                <td><?php echo cfs()->get('recruit-tbl06') ?></td>
              </tr>
              <tr>
                <th>勤務時間</th>
                <td><?php echo cfs()->get('recruit-tbl07') ?></td>
              </tr>
              <tr>
                <th>給与</th>
                <td><?php echo cfs()->get('recruit-tbl08') ?></td>
              </tr>
              <tr>
                <th>待遇</th>
                <td><?php echo cfs()->get('recruit-tbl09') ?></td>
              </tr>
              <tr>
                <th>休日・休暇</th>
                <td><?php echo cfs()->get('recruit-tbl10') ?></td>
              </tr>
              <tr>
                <th>その他</th>
                <td><?php echo cfs()->get('recruit-tbl11') ?></td>
              </tr>
            </table>
          </div><!-- /.tbl_wrap -->
          <p class="btn"><a href="<?php echo home_url('/'); ?>contact/" class="ophv"><img src="<?php echo home_url('/'); ?>assets-sp/img/recruit/btn.gif" width="516" height="73" alt="求人に関するお問い合わせ応募はこちら"></a></p>
        </section><!-- /#section02 -->
      </div><!-- /.inner -->
      <p class="to_top"><a href="#header">PAGE TOP</a></p>
    </main><!-- /#top -->

<?php get_footer(); ?>