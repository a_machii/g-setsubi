<?php get_header(); ?>

    <main id="top" class="main">
      <div class="inner">
        <div id="t_works" class="t_section">
          <section class="tw_section">
            <p class="img"><a href="<?php echo home_url('/'); ?>works/" class="ophv"><img src="<?php echo home_url('/'); ?>assets-sp/img/top/img01.jpg" width="215" height="130" alt="電気・設備工事の写真"></a></p>
            <div class="unit">
              <h2><a href="<?php echo home_url('/'); ?>works/">電気・設備工事</a></h2>
              <p>あらゆる建築物の電気・設備の設計から施工、そして保守・管理まで行っています。</p>
            </div><!-- /.unit -->
          </section><!-- /.tw_section -->

          <section class="tw_section">
            <p class="img"><a href="<?php echo home_url('/'); ?>works/" class="ophv"><img src="<?php echo home_url('/'); ?>assets-sp/img/top/img02.jpg" width="215" height="130" alt="自動制御設備の写真"></a></p>
            <div class="unit">
              <h2><a href="<?php echo home_url('/'); ?>sp/works/">自動制御設備</a></h2>
              <p>中央監視装置工事、自動制御設備工事、セキュリティー設備工事等を行っています。</p>
            </div><!-- /.unit -->
          </section><!-- /.tw_section -->

          <section class="tw_section">
            <p class="img"><a href="<?php echo home_url('/'); ?>works" class="ophv"><img src="<?php echo home_url('/'); ?>assets-sp/img/top/img03.jpg" width="215" height="130" alt="冷暖房設備の写真"></a></p>
            <div class="unit">
              <h2><a href="<?php echo home_url('/'); ?>sp/works/">冷暖房設備</a></h2>
              <p>マンション、商業施設等の冷暖房空調など新設工事、改修工事、メンテナンスを行っています。</p>
            </div><!-- /.unit -->
          </section><!-- /.tw_section -->

          <section class="tw_section">
            <p class="img"><a href="<?php echo home_url('/'); ?>works" class="ophv"><img src="<?php echo home_url('/'); ?>assets-sp/img/top/img04.jpg" width="215" height="130" alt="省エネ･ecoの写真"></a></p>
            <div class="unit">
              <h2><a href="<?php echo home_url('/'); ?>sp/works/">省エネ･eco</a></h2>
              <p>LED照明をはじめ、床暖房や業務用エアコンの改善など、省エネに関連した電気設備工事を行っています。</p>
            </div><!-- /.unit -->
          </section><!-- /.tw_section -->

          <section class="tw_section">
            <p class="img"><a href="works" class="ophv"><img src="<?php echo home_url('/'); ?>assets-sp/img/top/img05.jpg" width="215" height="130" alt="お水のトラブルの写真"></a></p>
            <div class="unit">
              <h2><a href="<?php echo home_url('/'); ?>sp/works/">お水のトラブル</a></h2>
              <p>当社は所沢市の指定水道工事店です。漏水や詰まりなど、お困りごとはお気軽にご相談ください。</p>
            </div><!-- /.unit -->
          </section><!-- /.tw_section -->

          <section class="tw_section">
            <p class="img"><a href="works" class="ophv"><img src="<?php echo home_url('/'); ?>assets-sp/img/top/img06.jpg" width="215" height="130" alt="お困りごとはありませんか？"></a></p>
            <div class="unit">
              <h2><a href="<?php echo home_url('/'); ?>/works/">お困りごとはありませんか？</a></h2>
              <p>IHキッチンの電磁波でお悩みの方や、水質改善に関心のある方へ。</p>
            </div><!-- /.unit -->
          </section><!-- /.tw_section -->
        </div><!-- /#tworks -->

        <section id="t_news" class="t_section">
          <h2><img src="<?php echo home_url('/'); ?>assets-sp/img/top/news_ttl.gif" width="136" height="34" alt="新着情報"></h2>
          <div class="article_area">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
            <article class="article">
              <dl>
                <dt><?php the_time('Y年m月d日') ?></dt>
                <dd><h3><?php the_title(); ?></h3></dd>
                <dd><?php the_content(); ?></dd>
              </dl>
            </article><!-- /.article -->
<?php endwhile; endif; ?>
          </div><!-- /article_area -->
        </section><!-- /#t_news -->
      </div><!-- /.inner -->
      <p class="to_top"><a href="#header">PAGE TOP</a></p>
    </main><!-- /#top -->

<?php get_footer(); ?>