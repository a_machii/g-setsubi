<?php /* Template Name: page */ ?>
<?php
  $page_id = $post->ID; //xxxに 固定ページIDを入力
  $content = get_page($page_id);
  $this_page_title = $content->post_title;
  $this_page_slug = $content->post_name;
?>
<?php get_header(); ?>

    <main id="contact" class="low main">
      <div class="inner">
        <section id="section01" class="l_section">
          <h3 class="h3ttl_a"><img src="<?php echo home_url('/'); ?>assets-sp/img/contact/h3ttl01.gif" width="680" height="65" alt="電話でのお問い合わせ"></h3>
          <div class="txt_area">
            <p>弊社の事業内容に関するご質問やご相談などがございましたらお気軽にお問い合わせください。</p>
            <p>お急ぎの方はお電話をご利用いただくとご案内がスムーズです。なお、電話番号のお間違いにはお気をつけください。</p>
          </div><!-- /.txt_area -->

          <div class="txt_area02">
            <p>●電話番号：03-3903-0963</p>
            <p>●受付時間：平日10：00～18：00</p>
            <p>（土・日・祝日および弊社指定休業日を除きます）</p>
          </div><!-- /.txt_area02 -->
          <p class="tel_btn"><a href="tel:0339030963" class="ophv"><img src="<?php echo home_url('/'); ?>assets-sp/img/contact/tel_btn.gif" width="555" height="74" alt="こちらをタップして電話をかける"></a></p>
        </section><!-- /#section01 -->

        <section id="section02" class="l_section">
          <h3 class="h3ttl_a"><img src="<?php echo home_url('/'); ?>assets-sp/img/contact/h3ttl02.gif" width="680" height="65" alt="会社情報"></h3>

          <!-- メールフォーム -->
          <form id="mailformpro" action="<?php echo home_url('/'); ?>mailform-sp/mailformpro/mailformpro.cgi" method="POST">
          <link rel="stylesheet" type="text/css" href="<?php echo home_url('/'); ?>mailform-sp/mfp.statics/mailformpro.css">
            <div class="cnt_tbl_wrap">
              <table>
                <tbody>
                  <tr>
                    <th>貴社名</th>
                    <td>
                      <input name="貴社名" type="text" />
                    </td>
                  </tr>
                  <tr>
                    <th class="req">お名前</th>
                    <td>
                      <input name="お名前" required="required" type="text" />
                    </td>
                  </tr>
                  <tr>
                    <th>フリガナ</th>
                    <td>
                      <input name="フリガナ" type="text" data-charcheck="kana" />
                    </td>
                  </tr>
                  <tr>
                    <th class="req">E-mail</th>
                    <td>
                      <input name="email" required="required" type="email" data-type="email" />
                    </td>
                  </tr>
                  <tr>
                    <th class="req">確認用E-mail</th>
                    <td>
                      <input name="確認用メールアドレス" required="required" type="email" data-type="email" />
                    </td>
                  </tr>
                  <tr>
                    <th>郵便番号</th>
                    <td>
                      <input type="text" name="郵便番号" data-address="都道府県,市区町村,丁目番地" />
                    </td>
                  </tr>
                  <tr>
                    <th>ご住所</th>
                    <td>
                      <ol>
                        <li>
                          <span>都道府県</span>
                          <select name="都道府県">
                            <option value="" selected="selected">【選択して下さい】</option>
                            <optgroup label="北海道・東北地方">
                              <option value="北海道">北海道</option>
                              <option value="青森県">青森県</option>
                              <option value="岩手県">岩手県</option>
                              <option value="秋田県">秋田県</option>
                              <option value="宮城県">宮城県</option>
                              <option value="山形県">山形県</option>
                              <option value="福島県">福島県</option>
                            </optgroup>
                            <optgroup label="関東地方">
                              <option value="栃木県">栃木県</option>
                              <option value="群馬県">群馬県</option>
                              <option value="茨城県">茨城県</option>
                              <option value="埼玉県">埼玉県</option>
                              <option value="東京都">東京都</option>
                              <option value="千葉県">千葉県</option>
                              <option value="神奈川県">神奈川県</option>
                            </optgroup>
                            <optgroup label="中部地方">
                              <option value="山梨県">山梨県</option>
                              <option value="長野県">長野県</option>
                              <option value="新潟県">新潟県</option>
                              <option value="富山県">富山県</option>
                              <option value="石川県">石川県</option>
                              <option value="福井県">福井県</option>
                              <option value="静岡県">静岡県</option>
                              <option value="岐阜県">岐阜県</option>
                              <option value="愛知県">愛知県</option>
                            </optgroup>
                            <optgroup label="近畿地方">
                              <option value="三重県">三重県</option>
                              <option value="滋賀県">滋賀県</option>
                              <option value="京都府">京都府</option>
                              <option value="大阪府">大阪府</option>
                              <option value="兵庫県">兵庫県</option>
                              <option value="奈良県">奈良県</option>
                              <option value="和歌山県">和歌山県</option>
                            </optgroup>
                            <optgroup label="四国地方">
                              <option value="徳島県">徳島県</option>
                              <option value="香川県">香川県</option>
                              <option value="愛媛県">愛媛県</option>
                              <option value="高知県">高知県</option>
                            </optgroup>
                            <optgroup label="中国地方">
                              <option value="鳥取県">鳥取県</option>
                              <option value="島根県">島根県</option>
                              <option value="岡山県">岡山県</option>
                              <option value="広島県">広島県</option>
                              <option value="山口県">山口県</option>
                            </optgroup>
                            <optgroup label="九州・沖縄地方">
                              <option value="福岡県">福岡県</option>
                              <option value="佐賀県">佐賀県</option>
                              <option value="長崎県">長崎県</option>
                              <option value="大分県">大分県</option>
                              <option value="熊本県">熊本県</option>
                              <option value="宮崎県">宮崎県</option>
                              <option value="鹿児島県">鹿児島県</option>
                              <option value="沖縄県">沖縄県</option>
                            </optgroup>
                          </select>
                        </li>
                      </ol>
                    </td>
                  </tr>
                  <tr>
                    <th>市区町村</th>
                    <td>
                      <input type="text" name="市区町村" />
                    </td>
                  </tr>
                  <tr>
                    <th>丁目番地</th>
                    <td>
                      <input type="text" name="丁目番地" />
                    </td>
                  </tr>
                  <tr>
                    <th class="req">連絡先電話番号</th>
                    <td>
                      <input name="連絡先電話番号" required="required" type="tel" data-type="tel" data-min="9" />
                    </td>
                  </tr>
                  <tr>
                    <th class="req">お問合せ種別</th>
                    <td>
                      <ul>
                        <label><input type="checkbox" name="件名" required="required" data-min="1" data-max="4" value="業務について" />&nbsp;業務について</label>
                        <label><input type="checkbox" name="件名" required="required" value="求人について" />&nbsp;求人について</label>
                        <label><input type="checkbox" name="件名" required="required" value="見積もり依頼" />&nbsp;見積もり依頼</label>
                        <label><input type="checkbox" name="件名" required="required" value="その他" />&nbsp;その他</label>
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <th>内容</th>
                    <td>
                      <textarea name="お問い合わせ内容" required="required" rows="10"></textarea>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div><!-- /.cnt_tbl_wrap -->

            <div class="btn_area">
              <button type="reset" class="mysubmit"><img src="<?php echo home_url('/'); ?>assets-sp/img/contact/reset_btn.png" width="590" height="94" alt="リセット"></button>
              <button class="mysubmit" type="submit"><img src="<?php echo home_url('/'); ?>assets-sp/img/contact/confirm_btn.png" width="590" height="94" alt="確認"></button>
            </div><!--/.confirm_btn-->

            <script id="mfpjs" src="<?php echo home_url('/'); ?>mailform-sp/mailformpro/mailformpro.cgi" type="text/javascript" charset="UTF-8"></script>
          </form>
        </section><!-- /#section02 -->

        <section id="section03" class="l_section">
          <h3 class="h3ttl_a"><img src="<?php echo home_url('/'); ?>assets-sp/img/contact/h3ttl03.gif" width="590" height="94" alt="個人情報保護方針"></h3>
          <p class="txt01">株式会社グリーン設備（以下「当社」という）は、高度通信社会における個人情報保護の重要性を認識し、以下の方針に基づき、個人情報の保護に努めます。</p>
          <section class="unit">
            <h4>1．個人情報の取得について</h4>
            <p>お客様が当ウェブサイトを閲覧されるにあたり、通常のご利用においては、住所、氏名などの個人情報をご提供いただく必要はありません。ただし、「お問い合わせ」のページから、当社へお問い合わせをする場合、氏名、ｅメールアドレスなどの必要な範囲の個人情報の提供をお願いしています。</p>
            <p>当社による個人情報の収集は、あくまでお客様の自発的な提供によるものであり、お客様が個人情報を提供された場合は、当社が個人情報の取り扱いに則って個人情報を利用する事をお客様が許諾されたものとします。</p>
          </section><!-- /.unit -->

          <section class="unit">
            <h4>2．個人情報の利用目的について</h4>
            <p>当社は、個人情報取得の際に示した利用目的の範囲内で、業務の遂行上必要な限りにおいて利用させていただき、それ以外の利用は致しません。</p>
          </section><!-- /.unit -->

          <section class="unit">
            <h4>3．個人情報の共有について</h4>
            <p>当社は、法令に定める場合を除き、個人情報をお客様の同意を得る事なく、第三者へ提供する事はありません。</p>
          </section><!-- /.unit -->

          <section class="unit">
            <h4>4．個人情報の管理について</h4>
            <p>当社は、個人情報の紛失、破壊、改ざん及び漏洩などを防止する為に、セキュリティの確保・向上に努めます。</p>
          </section><!-- /.unit -->

          <section class="unit">
            <h4>5．個人情報の開示・訂正・利用停止・消去について</h4>
            <p>当社は、お客様が自己の個人情報について、開示・訂正・利用停止・消去等を求める権利を有している事を確認し、これらの要求がある場合には、異議なく速やかに対応致します。尚、当社の個人情報の取り扱いにつきまして、ご意見、ご質問が御座いましたら、当社個人情報管理者までご連絡下さいます様、お願い申し上げます。</p>
          </section><!-- /.unit -->

          <section class="unit">
            <h4>6．個人情報保護コンプライアンス・プログラムの策定・実施・維持・改善</h4>
            <p>当社は、この方針を実行する為、個人情報保護コンプライアンス・プログラム（本方針、「個人情報保護規程」及びその他の規程、規則を含む）を策定し、これを当社全役員、従業員及びその他関係者に周知徹底させて実施し、維持し、継続的に改善致します。</p>
          </section><!-- /.unit -->

          <section class="unit">
            <h4>7．お問い合せ</h4>
            <p class="txt02">当社の個人情報の取扱に関するお問い合せは下記までご連絡ください。</p>
            <dl>
              <dt>株式会社グリーン設備</dt>
              <dd>〒359-1111埼玉県所沢市緑町3-21-8</dd>
              <dd>電話：04-2903-0963（本社代表）<br>FAX：04-2903-0968</dd>
            </dl>
          </section><!-- /.unit -->
        </section><!-- /#section03 -->
      </div><!-- /.inner -->
      <p class="to_top"><a href="#header">PAGE TOP</a></p>
    </main><!-- /#contact -->

<?php get_footer(); ?>